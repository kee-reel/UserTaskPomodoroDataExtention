#pragma once

#include <QtCore>

#include "../../Interfaces/Architecture/PluginBase/plugin_base.h"
#include "DataExtention.h"

class PomodoroManager : public QObject, public PluginBase
{
	Q_OBJECT
	Q_PLUGIN_METADATA(IID "PLAG.Plugin" FILE "PluginMeta.json")
	Q_INTERFACES(IPlugin)

public:
	PomodoroManager();
	virtual ~PomodoroManager() override;

private:
	ReferenceInstancePtr<IUserTaskDataExtention> m_userTask;
	DataExtention* m_dataExtention;
};
